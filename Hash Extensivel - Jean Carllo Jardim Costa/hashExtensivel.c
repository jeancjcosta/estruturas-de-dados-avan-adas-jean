#include"hashExtensivel.h"
#include<math.h>

int achar_indice(int chave, int bits){
    int indice = 0;
    int i = 0;
    while(i < bits && chave != 0){
        if(chave%2 == 1)
            indice += pow(2, i);
        chave /= 2;
        i++;
    }
    return indice;
}

//Cria uma hash vazia
Hash * hash_criar(int n, int bucket_tam){
    Hash * hash = (Hash*)malloc(sizeof(Hash));
    hash->n = n;
    hash->numBuckets = 0;
    hash->bits = 0;
    hash->bucket_tam = bucket_tam;
    hash->bucketList = NULL;
    return hash;
}

//insere uma chave em um bucket
Hash * hash_inserir(Hash * hash, int chave){
    if (hash->numBuckets == 0){ //Nao ha chaves adicionadas. primeira insercao
        hash->numBuckets = 2;
        hash->bits++;
        hash->bucketList = (Bucket**)realloc(hash->bucketList, hash->numBuckets*sizeof(Bucket*));
        Bucket * bucket = (Bucket*)malloc(sizeof(Bucket));
        bucket->indice = 1;
        bucket->lista = (int*)malloc(hash->bucket_tam*sizeof(int));
        bucket->lista[0] = chave;
        hash->bucketList[0] = bucket;
        hash->bucketList[1] = bucket;
    } else { //a partir da segunda insercao
        int indice = achar_indice(chave, hash->bits);
        if (hash->bucketList[indice]->indice < hash->bucket_tam){ //tem espaco no bucket
            hash->bucketList[indice]->lista[hash->bucketList[indice]->indice++] = chave;
        } else { //bucket cheio
            int i;
            int aux = indice - hash->numBuckets/2;
            if (aux < 0) aux = indice + hash->numBuckets/2;
            
            if (hash->bucketList[indice] != hash->bucketList[aux]) { //necessario dobrar a quantidade de indices
                hash->bits++;
                hash->numBuckets *= 2;
                hash->bucketList = (Bucket**)realloc(hash->bucketList, hash->numBuckets*sizeof(Bucket*));
                for(i = hash->numBuckets/2; i < hash->numBuckets; i++) {
                    hash->bucketList[i] = hash->bucketList[i-hash->numBuckets/2]; 
                }
                indice = achar_indice(chave, hash->bits);
                aux = indice - hash->numBuckets/2;
                if (aux < 0) aux = indice + hash->numBuckets/2;               
            } 
            Bucket * bucketA = (Bucket*)malloc(sizeof(Bucket));
            bucketA->indice = 1;
            bucketA->lista = (int*)malloc(hash->bucket_tam*sizeof(int));
            bucketA->lista[0] = chave;

            Bucket * bucketB = (Bucket*)malloc(sizeof(Bucket));
            bucketB->indice = 0;
            bucketB->lista = (int*)malloc(hash->bucket_tam*sizeof(int));
            
            Bucket * bucketC = hash->bucketList[indice];
            hash->bucketList[indice] = bucketA;
            hash->bucketList[aux] = bucketB;
            
            for (i = 0; i < bucketC->indice; i++)
                hash = hash_inserir(hash, bucketC->lista[i]);

            free(bucketC->lista);
            free(bucketC);
            
            
        }
    }
    
    return hash;
}

//busca uma chave na hash e retorna a chave se estiver o bucket ou -1 caso contrario
int hash_buscar(Hash * hash, int chave){
    int indice = achar_indice(chave, hash->bits);
    int i;
    Bucket * b = hash->bucketList[indice];
    for (i = 0; i < b->indice; i++){
        if (b->lista[i] == chave)
            return chave;
    }
    return -1;
}

//remove uma chave de um bucket
void hash_remover(Hash * hash, int chave){
    int indice = achar_indice(chave, hash->bits);
    int i;
    Bucket * b = hash->bucketList[indice];
    for(i = 0; i < b->indice; i++){
        if (chave == b->lista[i]){
            b->lista[i] = b->lista[--b->indice];
            printf("Elemento removido!\n");
            if (b->indice == 0){
                int aux = indice - hash->numBuckets/2;
                if (aux < 0) aux = indice + hash->numBuckets/2;
                free(b);
                hash->bucketList[indice] = hash->bucketList[aux]; 
            } 
        }
    }
}

//libera a hash
void hash_liberar(Hash * hash){
    int i;
    for (i = 0; i < hash->numBuckets; i++){  
        free(hash->bucketList[i]->lista);
    }
    for (i = 0; i < hash->numBuckets; i++){  
        free(hash->bucketList[i]);
    }
    free(hash);
}

