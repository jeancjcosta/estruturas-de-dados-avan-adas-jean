#include "hashExtensivel.h"

int main(){
    int n, tam_bucket;
    int chave;
    Hash * hash;
    
    printf("Informe o tamanho dos Buckets: ");
    scanf("%d", &tam_bucket);
    printf("Informe o valor maximo das chaves: ");
    scanf("%d", &n);
    
    hash = hash_criar(n,  tam_bucket);
    
    do{
        printf("Digite uma chave (0 para sair): ");
        scanf("%d", &chave);
        if (chave != 0) hash = hash_inserir(hash, chave);
    }while(chave != 0);
    printf("Digite uma chave para buscar: ");
    scanf("%d", &chave);
    if (hash_buscar(hash, chave) != -1)
        printf("Encontrou o elemento\n");
    else
        printf("Elemento nao encontrado\n");

    printf("Digite uma chave para remover: ");
    scanf("%d", &chave);

    hash_remover(hash, chave);

    hash_liberar(hash);
    return 0;
}
