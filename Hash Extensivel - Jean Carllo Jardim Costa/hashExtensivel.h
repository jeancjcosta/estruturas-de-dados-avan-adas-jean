#include<stdio.h>
#include<stdlib.h>

struct bucket{
    int * lista;
    int indice;
};

typedef struct bucket Bucket;

struct hash{
    int bucket_tam; //tamanho do bucket
    int n; //valor maximo das chaves;
    int numBuckets; //numero de buckets existentes
    int bits; //numero de bits significativos para achar o bucket
    Bucket ** bucketList;//lista de buckets
};

typedef struct hash Hash;

//Cria uma hash vazia
Hash * hash_criar(int n, int bucket_tam);

//insere uma chave em um bucket
Hash * hash_inserir(Hash * hash, int chave);

//busca uma chave na hash
int hash_buscar(Hash * hash, int chave);

//remove uma chave de um bucket
void hash_remover(Hash * hash, int chave);

//libera a hash
void hash_liberar(Hash * hash);


