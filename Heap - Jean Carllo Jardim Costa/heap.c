#include "heap.h"

static void troca(int a, int b, int* v){
    int f = v[a];
    v[a] = v[b];
    v[b] = f;
}


void corrige_abaixo(Heap* heap, int pos){
    int esq = pos*2+1;
    int dir = pos*2+2;
    if (dir < heap->pos) {
        if (heap->prioridade[esq] > heap->prioridade[dir] && heap->prioridade[esq] > heap->prioridade[pos]){
            troca(esq, pos, heap->prioridade);
            corrige_abaixo(heap, esq);    
        } else if (heap->prioridade[dir] > heap->prioridade[esq] && heap->prioridade[dir] > heap->prioridade[pos]){
            troca(esq, pos, heap->prioridade);
            corrige_abaixo(heap, esq);    
        }  
    }else if (esq < heap->pos){
        if (heap->prioridade[esq] > heap->prioridade[pos]){
            troca(esq, pos, heap->prioridade);
        }
    }    
}

void corrige_acima(Heap *heap, int pos){
    while (pos > 0){
        int pai = (pos-1)/2;
        if (heap->prioridade[pai] < heap->prioridade[pos])
            troca(pos, pai, heap->prioridade);
        else 
            break;
        pos = pai;
    }
}

Heap* heap_cria(int max){
    Heap* heap=(Heap*)malloc(sizeof(Heap));
    heap->max = max;
    heap->pos = 0;
    heap->prioridade = (int*)malloc(max*sizeof(int));
    return heap;
}

void  heap_insere(Heap* heap, int prioridade){
    if (heap->pos < heap->max){
        heap->prioridade[heap->pos] = prioridade;
        corrige_acima(heap, heap->pos);
        heap->pos++;
    } 
    else 
        printf("Heap CHEIO!\n%d nao inserido!\n", prioridade); 
}

int buscar(Heap* heap, int chave){
    int i;
    for(i = 0; i < heap->pos; i++){
        if (heap->prioridade[i] == chave)
            return i;
    }
    printf("%d nao encontrado!!!\n", chave);
    return -1;
}

int heap_remove(Heap* heap){
    if (heap->pos > 0) {
        int topo = heap->prioridade[0];
        heap->prioridade[0] = heap->prioridade[heap->pos - 1];
        heap->pos--;
        corrige_abaixo(heap, 0);
        return topo;
    } else {
        printf("Heap VAZIO!");
        return -1;
    }
}

void libera_heap(Heap* heap){
    free(heap);
}
