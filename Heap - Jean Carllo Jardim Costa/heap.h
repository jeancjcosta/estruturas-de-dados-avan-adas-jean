#include<stdio.h>
#include<stdlib.h>

struct _heap {
    int max; /*tamanho maximo do heap */
    int pos; /* proxima posicao disponivel no vetor */
    int* prioridade; /* vetor das prioridades */
};

typedef struct _heap Heap;

Heap* heap_cria(int max);
void  heap_insere(Heap* heap, int prioridade);
int buscar(Heap* heap, int chave);
int heap_remove(Heap* heap);
void libera_heap(Heap* heap);
