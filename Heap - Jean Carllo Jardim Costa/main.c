#include "heap.h"

int main(){
    printf("Criando heap de tamanho 10\n");
    Heap *heap = heap_cria(10);
    int indice;
    printf("inserindo 7\n");
    heap_insere(heap, 7);
    printf("inserindo 10\n");
    heap_insere(heap, 10);
    printf("inserindo 17\n");
    heap_insere(heap, 17);
    printf("inserindo 4\n");
    heap_insere(heap, 4);
    printf("inserindo 1\n");
    heap_insere(heap, 1);
    printf("inserindo 0\n");
    heap_insere(heap, 0);
    printf("inserindo 11\n");
    heap_insere(heap, 11);
    printf("inserindo 14\n");
    heap_insere(heap, 14);
    printf("inserindo 2\n");
    heap_insere(heap, 2);
    printf("inserindo 33\n");
    heap_insere(heap, 33);
    printf("inserindo 40\n");
    heap_insere(heap, 40);   
    
    indice = buscar(heap, 10);
    indice = buscar(heap, 40);
    indice = buscar(heap, 67);
    
    int valor = heap_remove(heap);
    printf("Topo removido = %d\n", valor);
    valor = heap_remove(heap);
    printf("Topo removido = %d\n", valor);
    
    libera_heap(heap);
    
    return 0;
}
