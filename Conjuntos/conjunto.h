#ifndef CONJUNTO_H
#define CONJUNTO_H

#include <string>
#include <iostream>
#include <bitset>

using namespace std;

class Conjunto
{
public:
	//construtor
	Conjunto(string id, int tam, string * str);

	//destrutor
	~Conjunto();

	//coloca um conjunto novo no lugar do atual
	void replace(int conj);

	//retorna o numero representando o conjunto 
	int getConjunto();

	//imprime todos os elementos do conjunto
	void print();

	//insere um elemento no conjunto
	void inserir(string c);

	//remove um elemento de um conjunto
	void remover(string c);

	//faz a união do conjunto atual com um conjunto fornecido.
	int uniao(Conjunto * conj);

	//faz a interseção do conjunto atual com um conjunto fornecido.
	int intersecao(Conjunto * conj);

	//subtrai o conjunto de enntrada do atual;
	int diferenca(Conjunto * conj);

	//verifica se o conjunto atual é subconjunto do conjunto fornecido
	void isSubset(Conjunto * conj);	

	//verifica se dois conjuntos sao iguais
	void isEqual(Conjunto * conj);

	//gera o complemento do conjunto atual;
	int complemento();

	//verifica se um elemente pertence ao conjunto
	void pertence(string str);

	//retorna o numero de elementos de um conjunto
	int getNumElementos();

private:
	int v; //inteiro que representa o conjunto
	string id; //identificador do conjunto
	int tam; //tamanho do vetor de bits
	string * str;
	int numElementos;
};

/*
11.  Recuperar o número de elementos de um determinado conjunto;
12.  Liberar a estrutura de dados (um determinado conjunto);*/

#endif