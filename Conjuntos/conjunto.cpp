#include "conjunto.h"

Conjunto::Conjunto(string id, int tam, string * str){
	this->v = 0;
	this->id = id;
	this->tam = tam;
	this->str = str;
	this->numElementos = 0;
}


Conjunto::~Conjunto(){
}

void Conjunto::replace(int conj){
	this->v = conj;
}

int Conjunto::getConjunto(){
	return v;
}

void Conjunto::print(){
	cout << "{ ";
	for(int i = 0; i < 26; i++){
		int j = 1;
		j = j << i;
		if (j & this->v)
			cout << str[i] << " " ;
	}
	cout << '}' << endl;
}

void Conjunto::inserir(string c){
	int j = 1;
	for (int i = 0; i < tam; i++){
		if (str[i] != c){
			j = j << 1; 
		} else {
			this->numElementos++;
			break;
		}
	}
	v = v|j;
}

void Conjunto::remover(string c){
	int j = 1;
	int i = 0;
	while (str[i] != c && i < tam){
		j = j << 1;
		i++;
	}
	if (j&v){
		v = v^j;
		this->numElementos--;
	}
}

int Conjunto::uniao(Conjunto * conj){
	return conj->getConjunto()|this->v;	
}

int Conjunto::intersecao(Conjunto * conj){
	return conj->getConjunto()&this->v;
}

int Conjunto::diferenca(Conjunto * conj){
	int a = conj->getConjunto();
	return (a|v)^a;
}	

void Conjunto::isSubset(Conjunto * conj){
	int a = conj->getConjunto();
	if (!((a|v)^a))
		cout << this->id << " e subconjunto de " << conj->id << endl;
	else
		cout << this->id << " nao e subconjunto de " << conj->id << endl;
}

void Conjunto::isEqual(Conjunto * conj){
	int a = conj->getConjunto();
	if (v == a)
		cout << this->id << " e " << conj->id << " sao iguais" << endl;
	else
		cout << this->id << " e " << conj->id << " nao sao iguais" << endl;
}

int Conjunto::complemento(){
	int a = 0;
	int j = 1;
	for (int i = 0; i < this->tam; i++){
		if (j&v){
			a = a;
		}else{
			a = a|j;
		}
		j = j << 1;
	}
	//cout << v << " " << a << " " << tam << endl;
	return a;
}

void Conjunto::pertence(string str){
	int j = 1;
	int i = 0;
	while (i < numElementos && this->str[i] != str){
		j = j << 1;
		i++;
	}

	if (j & v){
		cout << str << " pertence ao conjunto " << id << endl;
	}else {
		cout << str << " nao pertence ao conjunto " << id << endl;
	}
}

int Conjunto::getNumElementos(){
	return this->numElementos;
}