#include "conjunto.h"
#include <stdlib.h>
#include <time.h>

int main(){
	int numElementos;
	string * str;

	cout << "Informe a qtd de elementos a serem gerenciados: ";
	cin >> numElementos;
	str = new string[numElementos];
	for (int i = 0; i < numElementos; i++){
		cout << "Informe o " << i+1 << "o elementos a ser gerenciado: ";
		cin >> str[i];
	} 
	Conjunto * c1 = new Conjunto("C1", numElementos, str);
	Conjunto * c2 = new Conjunto("C2", numElementos, str);
	Conjunto * c3 = new Conjunto("C3", numElementos, str);
	Conjunto * c4 = new Conjunto("C4", numElementos, str);

	cout << "Criando conjunto C1...\n"<< endl;
	cout << "Criando conjunto C2...\n" <<endl;
	cout << "Criando conjunto C3...\n" <<endl;
	
	int op = 1;
	cout << "Insira elementos...\n";
	while(op != 0){
		cout << "1 - Inserir elemento em C1\n";
		cout << "2 - Inserir elemento em C2\n";
		cout << "3 - Inserir elemento em C3\n";
		cout << "0 - sair\n";
		cin >> op;
		if (op == 0) break;
		cout << "digite o elemento: ";
		string s;
		cin >> s;
		if (op == 1){
			c1->inserir(s);
		}else if (op == 2){
			c2->inserir(s);
		}else if (op == 3){
			c3->inserir(s);
		}
	}
	cout << "C1 = ";
	c1->print();
	cout << "C2 = ";
	c2->print();
	cout << "C3 = ";
	c3->print();

	cout << "Remova elementos...\n";
	op = 1;
	while(op != 0){
		cout << "1 - remover elemento em C1\n";
		cout << "2 - remover elemento em C2\n";
		cout << "3 - remover elemento em C3\n";
		cout << "0 - sair\n";
		cin >> op;
		if (op == 0) break;
		cout << "digite o elemento: ";
		string s;
		cin >> s;
		if (op == 1){
			c1->remover(s);
		}else if (op == 2){
			c2->remover(s);
		}else if (op == 3){
			c3->remover(s);
		}
	}

	cout << "C1 = ";
	c1->print();
	cout << "C2 = ";
	c2->print();
	cout << "C3 = ";
	c3->print();

	c4->replace(c1->uniao(c2));
	cout << "C1 uniao C2 = ";
	c4->print();

	c4->replace(c1->intersecao(c2));
	cout << "C1 intersecao C2 = ";
	c4->print();

	c4->replace(c1->diferenca(c2));
	cout << "C1 - C2 = ";
	c4->print();

	c4->replace(c2->diferenca(c1));
	cout << "C2 - C1 = ";
	c4->print();

	c3->isSubset(c2);
	c2->isSubset(c3);
	
	c2->isEqual(c3);
	c2->isEqual(c1);

	cout << "complemento de C1 = ";
	c4->replace(c1->complemento());
	c4->print();

	c2->pertence(str[0]);
	c2->pertence(str[2]);

	cout << "O conjunto C3 tem " << c3->getNumElementos() << " elementos\n";

	delete c1;
	delete c2;
	delete c3;
	delete c4;

	return 0;
}