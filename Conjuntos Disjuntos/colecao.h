#ifndef CDISJUNTO_H
#define CDISJUNTO_H

#include <stdlib.h>
#include <iostream>

using namespace std;

struct no{
	struct no * representante;
	struct no * prox;
	int info;
};

typedef struct no No;

class Colecao //conjunto disjunto
{
public:
	//construtor
	Colecao(int N);

	//destrutor
	~Colecao();

	//cria  conjunto de elemento único
	void MakeSet(No * x);

	//faz a união de dois conjuntos
	void Union(No * x, No * y);

	//encontra o representante do conjunto que contém x
	No * FindSet(No * x);

	//verifica se x e y pertencem ao mesmo conjunto
	void SameSet(No * x, No * y);

	//retorn lista de todos elementos
	No ** getLista();

private:
	No ** lista;//lista de elementos
	int N;//numero de elementos
};


#endif