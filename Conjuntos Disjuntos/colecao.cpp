#include "colecao.h"


//construtor
Colecao::Colecao(int N){
	this->N = N;
	this->lista = new No*[N];
	for (int i = 0; i < N; ++i)
	{
		lista[i] = new No;
		lista[i]->info = i+1;
	}
}

//destrutor
Colecao::~Colecao(){
	for (int i = 0; i < N; ++i)
	{
		delete lista[i];
	}
	delete lista;
}

//retorn lista de todos elementos
No ** Colecao::getLista(){
	return this->lista;
}

//cria  conjunto de elemento único
void Colecao::MakeSet(No * x){
	x->representante = x;
	x->prox = NULL;
}

//faz a união de dois conjuntos com y sendo o novo representante
void Colecao::Union(No * x, No * y){
	x = x->representante;
	y = y->representante;
	No * aux = y;
	if (x != y){
		while (y->prox != NULL){
			y = y->prox;
		}
		y->prox = x;
		do{
			x->representante = aux;
			x = x->prox;
		}while(x != NULL);
	}
}

//encontra o representante do conjunto que contém x
No * Colecao::FindSet(No * x){
	return x->representante;
}

//verifica se x e y pertencem ao mesmo conjunto
void Colecao::SameSet(No * x, No * y){
	if (x->representante == y->representante)
		cout << x->info << " e " << y->info << " pertencem ao mesmo conjunto.\n";
	else
		cout << x->info << " e " << y->info << " nao pertencem ao mesmo conjunto.\n";
}