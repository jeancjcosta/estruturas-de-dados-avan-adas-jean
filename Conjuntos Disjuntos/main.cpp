#include "colecao.h"
#include <time.h>

int main(){
	int N;
	cout << "Informe o numero de elementos disponíveis\n";
	cin >> N;
	Colecao * c = new Colecao(N); 
	srand(time(NULL));
	cout << "criando N conjuntos disjuntos...\n";
	for (int i = 0; i < N; ++i)
	{
		c->MakeSet(c->getLista()[i]);
	}

	//fazendo 10 unioes aleatorias
	cout << "fazendo uniões aleatorias.\n";
	for (int i = 0; i < 10; i++){
		c->Union(c->getLista()[rand()%N], c->getLista()[rand()%N]);
	}

	int x = rand()%N;
	int y = rand()%N;

	cout << "Verificando se " << x+1 << " e " << y+1 << " pertencem ao mesmo conjunto\n";
	No * X = c->getLista()[x];
	No * Y = c->getLista()[y];

	c->SameSet(X, Y);

	delete c;

	return 0;
}

