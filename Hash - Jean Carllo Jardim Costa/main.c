#include"hash.h"

int main(){
    Hash * hash;
    hash = hash_criar(37);
    
    printf("Inserindo 37\n");
    hash_inserir(hash, 37);
    printf("Inserindo 74\n");
    hash_inserir(hash, 74);
    printf("Inserindo 1000\n");
    hash_inserir(hash, 1000);
    printf("Inserindo 3413\n");
    hash_inserir(hash, 3413);
    printf("Inserindo 55667788\n");
    hash_inserir(hash, 55667788);
    printf("Inserindo 3\n");
    hash_inserir(hash, 3);
    printf("Inserindo 40\n");
    hash_inserir(hash, 40);
    
    Lista * lst = hash_buscar(hash, 40);
    if (lst != NULL) printf("Busca = %d\n", lst->info);
    else printf("Valor nao encontrado!\n");
    lst = hash_buscar(hash, 3413);
    if (lst != NULL)printf("Busca = %d\n", lst->info);
    else printf("Valor nao encontrado!\n");
    lst = hash_buscar(hash, 22);
    if (lst != NULL)printf("Busca = %d\n", lst->info);
    else printf("22 nao encontrado!\n");
    
    printf("Removendo 74\n");
    hash_remover(hash, 74);
    
    hash_liberar(hash);
    return 0;
}
