#include<stdio.h>
#include<stdlib.h>
#include"listaEncadeada.h"


struct hash{
    int max;
    Lista ** tabela;
};

typedef struct hash Hash;

//Cria uma hash vazia com max posições e encadeamento externo
Hash * hash_criar(int max);

//Insere um valor na hash
void hash_inserir(Hash *hash, int chave);

//Busca um elemento na hash com a chave indicada
Lista * hash_buscar(Hash *hash, int chave);

//remove um elemento da hash 
void hash_remover(Hash *hash, int chave);

//Libera a hash com suas respectivas listas externas
void hash_liberar(Hash *hash);
