#include"hash.h"

Hash * hash_criar(int max){
    int i;
    Hash *hash = (Hash*)malloc(max*sizeof(Hash));//aloca espaço para uma hash
    hash->max = max;//tamanho maximo assumido pela hash (chaves)
    hash->tabela = (Lista**)malloc(max*sizeof(Lista*));//aloca uma lista de lista para encadeamento externo
    for(i = 0; i < max; i++){
        hash->tabela[i] = lst_cria(); //aloca as listas
    }
    return hash;
}

void hash_inserir(Hash *hash, int chave){
    int pos = chave%hash->max;
    Lista * lst = hash->tabela[pos];
    hash->tabela[pos] = lst_insere(lst, chave);
}

Lista * hash_buscar(Hash *hash, int chave){
    Lista * lst = hash->tabela[chave%hash->max];
    lst = lst_busca(lst, chave);
    return lst; 
}

void hash_remover(Hash *hash, int chave){
    Lista * lst = hash->tabela[chave%hash->max];
    hash->tabela[chave%hash->max] = lst_retira(lst, chave);
}

void hash_liberar(Hash *hash){
    int i;
    for (i = 0; i < hash->max; i++){
        lst_libera(hash->tabela[i]);
    }
    free(hash);
}
