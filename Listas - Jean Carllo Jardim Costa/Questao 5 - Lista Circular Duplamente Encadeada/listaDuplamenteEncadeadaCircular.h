#include <stdlib.h>
#include <stdio.h>

struct lista {
    int info;
    struct lista* prox;
    struct lista* ant;
};
typedef struct lista Lista;

/*1 função de criação: retorna uma lista vazia */
Lista* lst_cria (void);

/*2 inserção no início: retorna a lista atualizada */
Lista* lst_insere (Lista* lst, int val);

/*3 função imprime: imprime valores dos elementos */
void lst_imprime (Lista* lst);

/* 4 função imprime recursiva original */
void lst_imprime_rec (Lista* lst, Lista* aux);

/* 5 função vazia: retorna 1 se vazia ou 0 se não vazia */
int lst_vazia (Lista* lst);

/* 6 função lst_busca: busca um elemento na lista */
Lista* lst_busca (Lista* lst, int val);

/* 7 retira: retira elemento da lista */
Lista* lst_retira (Lista* lst, int val);

/* 8 Função retira recursiva */
Lista* lst_retira_rec (Lista* lst, Lista* aux, int val);

/* 9 Liberar lista: Função que libera a lista */
void lst_libera (Lista* lst);
