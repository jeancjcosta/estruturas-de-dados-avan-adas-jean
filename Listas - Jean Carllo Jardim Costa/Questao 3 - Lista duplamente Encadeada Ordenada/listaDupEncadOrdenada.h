#include <stdlib.h>
#include <stdio.h>

struct lista {
    int info;
    struct lista* prox;
    struct lista* ant;
};
typedef struct lista Lista;

/*1 função de criação: retorna uma lista vazia */
Lista* lst_cria (void);

/*2 inserção no início: retorna a lista atualizada */
Lista* lst_insere_ordenado (Lista* lst, int val);

/*3 função imprime: imprime valores dos elementos */
void lst_imprime (Lista* lst);

/* 4 função imprime recursiva original */
void lst_imprime_rec (Lista* lst);

/* 5 função imprime recursiva invertida */
void lst_imprime_rec_inv (Lista* lst);

/* 6 função vazia: retorna 1 se vazia ou 0 se não vazia */
int lst_vazia (Lista* lst);

/* 7 função lst_busca: busca um elemento na lista */
Lista* lst_busca (Lista* lst, int val);

/* 8 retira: retira elemento da lista */
Lista* lst_retira (Lista* lst, int val);

/* 9 Função retira recursiva */
Lista* lst_retira_rec (Lista* lst, int val);

/* 10 Liberar lista: Função que libera a lista */
void lst_libera (Lista* lst);

/* 11 Lista igual: verofica se duas listas são iguais */
int lst_igual (Lista* lst1, Lista* lst2);
