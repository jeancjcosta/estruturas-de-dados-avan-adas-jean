#include "listaEncadeadaOrdenada.h"


int main(){
    Lista* lst;
    Lista* lst2;
    Lista* elem;
    
    lst=lst_cria();
    lst2=lst_cria();
    
    lst=lst_insere_ordenado(lst,10);
    lst=lst_insere_ordenado(lst,5);
    lst=lst_insere_ordenado(lst,2);
    lst=lst_insere_ordenado(lst,70);
    
    lst2=lst_insere_ordenado(lst2,1);
    lst2=lst_insere_ordenado(lst2,5);
    lst2=lst_insere_ordenado(lst2,3);
    lst2=lst_insere_ordenado(lst2,7);
    
    if (lst_igual(lst, lst2))
        printf("As listas são iguais.\n");
    else
        printf("As listas são diferentes.\n");
    
    lst_imprime(lst);
    lst_imprime_rec(lst);
    printf("Fim\n");
    lst_imprime_rec_inv(lst);
    printf("Fim\n");
    elem=lst_busca(lst, 78);
    lst=lst_retira(lst,78);
    lst_imprime(lst);
    lst=lst_retira_rec(lst,45);
    lst_imprime(lst);
    
    lst_libera(lst);
    lst_libera(lst2);
    
    return 0;
}
