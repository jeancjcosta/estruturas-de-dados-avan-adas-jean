
#include "listaEncadeadaCircular.h"

/*1 função de criação: retorna uma lista vazia - ok*/
Lista* lst_cria (void){
    return NULL;
}


/* 2 insere: insere elemento na lista - ok*/
Lista* lst_insere (Lista* lst, int val){
    Lista* novo;
    Lista* ant = NULL; /* ponteiro para elemento anterior*/
    Lista* p = lst; /* ponteiro para percorrer a lista */
    
    
    /* cria novo elemento */
    novo = (Lista*) malloc(sizeof(Lista));
    novo->info = val;
    
    if (lst == NULL){
        lst = novo;
        lst->prox = lst;
    } else {
        while(p->prox != lst)
            p = p->prox;
        novo->prox = lst;
        p->prox = novo;
        lst = novo;
    }
    
    return lst; /* retorna ponteiro para o primeiro elemento*/
}

/*3 função imprime: imprime valores dos elementos - ok */
void lst_imprime (Lista* lst){
    Lista* p;
    if (lst == NULL){
        printf("fim\n");
    } else {
        p = lst;
        do {
            printf("info = %d\n", p->info);
            p = p->prox;
        } while (p != lst);
        printf("fim\n");
    } 
}

/* 4 função imprime recursiva original */
void lst_imprime_rec (Lista* lst, Lista* aux){
    if (!lst_vazia(lst)) { /* imprime primeiro elemento */
        printf("info: %d\n",aux->info); /* imprime sub-lista */
        if (aux->prox != lst)
            lst_imprime_rec(lst, aux->prox);
    }
}


/* 5 função vazia: retorna 1 se vazia ou 0 se não vazia - ok*/
int lst_vazia (Lista* lst){
    return (lst == NULL);
}

/* 6 função lst_busca: busca um elemento na lista - ok*/
Lista* lst_busca (Lista* lst, int val){ 
    Lista* p = lst;
    do {
        if (p != NULL && p->info == val)
            return p;
        p = p->prox;
    } while (p != lst);
    return NULL; /* não achou o elemento */
}


/* 7 retira: retira elemento da lista - ok*/
Lista* lst_retira (Lista* lst, int val){
    Lista* ant = NULL; /* ponteiro para elemento anterior */
    Lista* p = lst;    /* ponteiro para percorrer a lista */
    
    if (p != NULL && p->info == val) { /*verifica se é o primeiro elemento da lista */
        while (p->prox != lst)
            p = p->prox;
        p->prox = lst->prox;
        free(lst);
        lst = p;
        return lst;
    } 
    if (p == NULL) {//lista vazia
        return lst;
    }
    p = p->prox;
    /* procura elemento na lista, guardando anterior */
    while (p != lst && p->info != val) {
        ant = p;
        p = p->prox;
    }
    
    /* achou: retira */
    if (p->info == val) { /* retira elemento do meio da lista */
        ant->prox = p->prox;
        free(p); /* libera espaço ocupado pelo elemento */
    }
    return lst;
}

/* 8 Função retira recursiva */
Lista* lst_retira_rec (Lista* lst, Lista* aux, int val, int primRec){
    if (!lst_vazia(lst)) { /* verifica se elemento a ser retirado é o primeiro */
        if (lst->info == val) {
            Lista* t = lst; /* para poder liberar */
            lst = lst->prox;
            if (primRec){ //se for o primeiro elemento da lista então liga o ultimo ao segundo.
                Lista* lstAux = lst;
                while(lstAux->prox != aux)
                    lstAux = lstAux->prox;
                lstAux->prox = aux->prox; 
            }
            free(t);
        } else if (lst->prox != aux){/* retira de sub-lista */
            lst->prox = lst_retira_rec(lst->prox, aux, val, FALSE);
        }
    }
    return lst;
}

/* 9 Liberar lista: Função que libera a lista - ok*/
void lst_libera (Lista* lst){
    Lista* p = lst;
    Lista* t;
    do {
        t = p; 
        p = p->prox;/* guarda referência para o próximo elemento */
        free(t); /* libera a memória apontada por t */
    } while (p != lst);
}

