#include "listaEncadeadaCircular.h"


int main(){
    Lista* lst;
    Lista* elem;
    
    lst=lst_cria();
   
    lst=lst_insere(lst,10);
    lst=lst_insere(lst,2);
    lst=lst_insere(lst,20);
    lst=lst_insere(lst,17);
    
    printf("FIM\n");
    lst_imprime(lst);
    lst_imprime_rec(lst, lst);
    printf("Fim\n");
    elem=lst_busca(lst, 10);
    lst=lst_retira(lst,10);
    lst_imprime(lst);
    lst=lst_retira_rec(lst, lst, 17, TRUE);
    
    lst_imprime(lst);
    
    lst_libera(lst);
    return 0;
}
