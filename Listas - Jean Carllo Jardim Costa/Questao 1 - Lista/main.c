#include "listaEncadeada.h"


int main(){
    Lista* lst;
    Lista* elem;
    lst=lst_cria();
    lst=lst_insere(lst,23);
    lst=lst_insere(lst,45);
    lst=lst_insere(lst,56);
    lst=lst_insere(lst,78);
    lst_imprime(lst);
    lst_imprime_rec(lst);
    printf("Fim\n");
    lst_imprime_rec_inv(lst);
    printf("Fim\n");
    elem=lst_busca(lst, 78);
    lst=lst_retira(lst,78);
    lst_imprime(lst);
    lst=lst_retira_rec(lst,45);
    lst_imprime(lst);
    lst_libera(lst);
    return 0;
}
