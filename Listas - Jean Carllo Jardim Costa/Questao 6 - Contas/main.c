#include "listaConta.h"

int menu(){
    int opcao;
    printf("========================== MENU ===============================\n");
    printf ("1.   Inserir uma conta bancária;\n");
    printf ("2.   Inserir uma conta poupança;\n");
    printf ("3.   Inserir uma conta fidelidade;\n");
    printf ("4.   Realizar crédito em uma determinada conta;\n");
    printf ("5.   Realizar débito em uma determinada conta;\n");
    printf ("6.   Consultar o saldo de uma conta;\n");
    printf ("7.   Consultar o bônus de uma conta fidelidade;\n");
    printf ("8.   Realizar uma transferência entre duas contas;\n");
    printf ("9.   Render juros de uma conta poupança;\n");
    printf ("10.  Render bônus de uma conta fidelidade;\n");
    printf ("11.  Remover uma conta;\n");
    printf ("12.  Imprimir número e saldo de todas as contas cadastradas;\n");
    printf ("13.  Sair.\n");
    printf("===============================================================\nopcao: ");
    scanf("%d", &opcao);
    return opcao;
}

int main(){
    Lista* lst;
    int opcao;
    int numeroConta;
    int conta1;
    int conta2;
    double valor;
    Lista* elem;
    Lista* lst1;
    Lista* lst2;
    lst = lst_cria();
    
    do{
        opcao = menu();
        
        switch(opcao){
            case 1: //inserir conta bancaria
                printf("Informe o numero da conta: ");
                scanf("%d", &numeroConta);
                Bancaria *contaB = new_conta_bancaria(numeroConta);
                lst = lst_insere_ordenado(lst, BANCARIA, contaB);
                break;
            case 2: //inserir conta poupanca
                printf("Informe o numero da conta: ");
                scanf("%d", &numeroConta);
                Poupanca *contaP = new_conta_poupanca(numeroConta);
                lst = lst_insere_ordenado(lst, POUPANCA, contaP);
                break;
            case 3://inserir conta fidelidade
                printf("Informe o numero da conta: ");
                scanf("%d", &numeroConta);
                Fidelidade *contaF = new_conta_fidelidade(numeroConta);
                lst = lst_insere_ordenado(lst, FIDELIDADE, contaF);
                break;
            case 4://realizar credito em uma conta
                
                printf("Informe o numero da conta: ");
                scanf("%d", &numeroConta);
                printf("Informe o valor a ser creditado: ");
                scanf("%lf", &valor); 
                elem = lst_busca (lst, numeroConta);
                if (elem != NULL && elem->tipo == POUPANCA){
                    Poupanca* contaPoup = elem->conta;
                    contaPoup->saldo = credito(contaPoup->saldo, valor);
                } else if (elem != NULL && elem->tipo == BANCARIA){
                    Bancaria* contaBanc = elem->conta;
                    contaBanc->saldo = credito(contaBanc->saldo, valor);
                } else if (elem != NULL && elem->tipo == FIDELIDADE){
                    Fidelidade* contaFid = elem->conta;
                    contaFid->saldo = creditoFidelidade(contaFid->saldo, &contaFid->bonus, valor);
                } else {
                    printf ("Conta nao localizada.\n");
                }
                break;
            case 5://realizar debito em uma conta
                printf("Informe o numero da conta: ");
                scanf("%d", &numeroConta);
                printf("Informe o valor a ser debitado: ");
                scanf("%lf", &valor); 
                elem = lst_busca (lst, numeroConta);
                if (elem != NULL && elem->tipo == POUPANCA){
                    Poupanca* contaPoup = elem->conta;
                    contaPoup->saldo = debito(contaPoup->saldo, valor);
                } else if (elem != NULL && elem->tipo == BANCARIA){
                    Bancaria* contaBanc = elem->conta;
                    contaBanc->saldo = debito(contaBanc->saldo, valor);
                } else if (elem != NULL && elem->tipo == FIDELIDADE){
                    Fidelidade* contaFid = elem->conta;
                    contaFid->saldo = debito(contaFid->saldo, valor);
                } else {
                    printf ("Conta nao localizada.\n");
                }
                break;
            case 6://consultar saldo de uma conta
                printf("Informe o numero da conta: ");
                scanf("%d", &numeroConta);
                 
                elem = lst_busca (lst, numeroConta);
                if (elem != NULL && elem->tipo == POUPANCA){
                    Poupanca* contaPoup = elem->conta;
                    printf("Saldo: %.2lf\n", contaPoup->saldo);
                } else if (elem != NULL && elem->tipo == BANCARIA){
                    Bancaria* contaBanc = elem->conta;
                    printf("Saldo: %.2lf\n", contaBanc->saldo);
                } else if (elem != NULL && elem->tipo == FIDELIDADE){
                    Fidelidade* contaFid = elem->conta;
                    printf("Saldo: %.2lf\n", contaFid->saldo);
                } else {
                    printf ("Conta nao localizada.\n");
                }
                break;
            case 7: //consulta bonus de uma conta fidelidade
                printf("Informe o numero da conta Fidelidade: ");
                scanf("%d", &numeroConta);
                elem = lst_busca (lst, numeroConta);
                Fidelidade* contaFid = elem->conta;
                printf("Bonus: %.2lf\n", contaFid->bonus);
                break;
            case 8:
                printf("Informe a conta para debito: ");
                scanf("%d", &conta1);
                printf("Informe a conta para credito: ");
                scanf("%d", &conta2);
                printf("Informe o valor: ");
                scanf("%lf", &valor);
                lst1 = lst_busca(lst, conta1);
                lst2 = lst_busca(lst, conta2);
                
                if(lst1 == NULL || lst2 == NULL){
                    printf("Alguma conta é inválida.\n");
                }else{
                    //realiza o debito
                    if (lst1->tipo == POUPANCA){
                        Poupanca* contaPoup = lst1->conta;
                        contaPoup->saldo = debito(contaPoup->saldo, valor);
                    } else if (lst1->tipo == BANCARIA){
                        Bancaria* contaBanc = lst1->conta;
                        contaBanc->saldo = debito(contaBanc->saldo, valor);
                    } else if (lst1->tipo == FIDELIDADE){
                        Fidelidade* contaFid = lst1->conta;
                        contaFid->saldo = debito(contaFid->saldo, valor);
                    }
                    
                    //realiza o credito
                    if (lst2->tipo == POUPANCA){
                        Poupanca* contaPoup = lst2->conta;
                        contaPoup->saldo = credito(contaPoup->saldo, valor);
                    } else if (lst2->tipo == BANCARIA){
                        Bancaria* contaBanc = lst1->conta;
                        contaBanc->saldo = credito(contaBanc->saldo, valor);
                    } else if (lst2->tipo == FIDELIDADE){
                        Fidelidade* contaFid = lst2->conta;
                        contaFid->saldo = credito(contaFid->saldo, valor);
                    }    
                }
                
                break;
            case 9://render juros
                printf("Informe uma conta poupanca para render 1%% juros: ");
                scanf("%d", &numeroConta);
                elem = lst_busca(lst, numeroConta);
       
                if (elem != NULL && elem->tipo == POUPANCA){
                    Poupanca* contaPoup = elem->conta;
                    contaPoup->saldo = renderJuros(contaPoup->saldo, 0.01);
                    printf("Saldo: %lf\n", contaPoup->saldo);
                } else{
                    printf("Conta nao localizada.\n");
                }
                
                break;
            case 10: // render bonus
                printf("Informe uma conta fidelidade para render bonus: ");
                scanf("%d", &numeroConta);
                elem = lst_busca(lst, numeroConta);
       
                if (elem != NULL && elem->tipo == FIDELIDADE){
                    Fidelidade* contaFid = elem->conta;
                    contaFid->saldo = renderBonus(contaFid->saldo, &contaFid->bonus);
                    
                } else{
                    printf("Conta nao localizada.\n");
                }
                break;
            case 11: //remover uma conta
                printf("Informe o numero da conta a ser removida: ");
                scanf("%d", &numeroConta);
                lst = lst_retira(lst, numeroConta);
                break;
            case 12: //imprime todos os saldos e contas
                lst_imprime(lst);
                break;
            default:
                break;
        }
    }while(opcao != 13);
    
    return 0;
}
