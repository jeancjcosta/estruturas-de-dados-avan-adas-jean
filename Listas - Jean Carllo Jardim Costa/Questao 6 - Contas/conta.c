#include "conta.h"


//cria nova conta bancaria
Bancaria* new_conta_bancaria(int numero){
    Bancaria *novo = (Bancaria*)malloc(sizeof(Bancaria));
    novo->saldo = 0;
    novo->numero = numero;
}

//cria nova conta poupanca
Poupanca* new_conta_poupanca(int numero){
    Poupanca *novo = (Poupanca*)malloc(sizeof(Poupanca));
    novo->saldo = 0;
    novo->numero = numero;
}

//cria nova coonta fidelidade
Fidelidade* new_conta_fidelidade(int numero){
    Fidelidade *novo = (Fidelidade*)malloc(sizeof(Fidelidade));
    novo->saldo = 0;
    novo->numero = numero;
    //novo->bonus = 0;
}

//efetua credito em conta e retorna o novo saldo
double credito(double saldo, double valorCreditado){
    return saldo+valorCreditado;
}

//efetua debito em conta e retorna o novo saldo
double debito(double saldo, double valorDebitado){
    return saldo-valorDebitado;
}

//efetua credito em conta fidelidade
double creditoFidelidade(double saldo, double *bonus, double valorCreditado){
    *bonus += valorCreditado*0.01;
    return saldo + valorCreditado;
}

//efetua a taxa de jurus e retorna o novo saldo
double renderJuros(double saldo, double taxa){
    return saldo*(1.0+taxa);
}

//adiciona o bonus ao saldo e zera o bonus
double renderBonus(double saldo, double *bonus){
    double novoSaldo = saldo+(*bonus);
    *bonus = 0;
    return novoSaldo;
}
