#ifndef LISTACONTA_H
#define LISTACONTA_H

#include <stdlib.h>
#include <stdio.h>
#include "conta.h"

struct lista{
    int tipo;
    int info;
    struct lista *prox;
    void *conta;
};

typedef struct lista Lista;

/*1 função de criação: retorna uma lista vazia ok */
Lista* lst_cria (void);

/*2 inserção ordenada: retorna a lista atualizada ok */
Lista* lst_insere_ordenado (Lista* lst, int tipo, void *conta);

/*3 função imprime: imprime valores dos elementos ok*/
void lst_imprime (Lista* lst);

/* 4 função imprime recursiva original ok*/
void lst_imprime_rec (Lista* lst);

/* 5 função imprime recursiva invertida */
void lst_imprime_rec_inv (Lista* lst);

/* 6 função vazia: retorna 1 se vazia ou 0 se não vazia */
int lst_vazia (Lista* lst);

/* 7 função lst_busca: busca um elemento na lista */
Lista* lst_busca (Lista* lst, int val);

/* 8 retira: retira elemento da lista */
Lista* lst_retira (Lista* lst, int val);

/* 9 Função retira recursiva */
Lista* lst_retira_rec (Lista* lst, int val);

/* 10 Liberar lista: Função que libera a lista */
void lst_libera (Lista* lst);

/* 11 Lista igual: verofica se duas listas são iguais */
int lst_igual (Lista* lst1, Lista* lst2);


#endif
