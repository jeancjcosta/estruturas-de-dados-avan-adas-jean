#ifndef CONTA_H
#define CONTA_H
#include <stdlib.h>
#include <stdio.h>

#define BANCARIA 1
#define POUPANCA 2
#define FIDELIDADE 3

struct bancaria {
    int numero;
    double saldo;
};

typedef struct bancaria Bancaria;

struct poupanca {
    int numero;
    double saldo;
};

typedef struct poupanca Poupanca;

struct fidelidade {
    int numero;
    double saldo;
    double bonus;
};

typedef struct fidelidade Fidelidade;

//cria nova conta bancaria
Bancaria* new_conta_bancaria(int numero);

//cria nova conta poupanca
Poupanca* new_conta_poupanca(int numero);

//cria nova coonta fidelidade
Fidelidade* new_conta_fidelidade(int numero);

//efetua credito em conta e retorna o novo saldo
double credito(double saldo, double valorCreditado);

//efetua debito em conta e retorna o novo saldo
double debito(double saldo, double valorDebitado);

//efetua credito em conta fidelidade
double creditoFidelidade(double saldo, double *bonus, double valorCreditado);

//efetua a taxa de jurus e retorna o novo saldo
double renderJuros(double saldo, double taxa);

//adiciona o bonus ao saldo e zera o bonus
double renderBonus(double saldo, double *bonus);

#endif
